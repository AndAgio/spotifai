# gnn4ifa_data

This submodule contains the dataset of IFAs simulated using ndnSIM (https://ndnsim.net/current/). This dataset is collected implementing two type of IFAs:
- IFA where the attacker(s) request content which is under the prefix served by the producer, i.e., IFA for existing content
- IFA where the attacker(s) request content which is not under the prefix served by the producer, i.e., IFA for non existing content

***

## Structure

This dataset includes three scenarios:
* Normal
* IFA_4_Existing
* IFA_4_Non_Existing

For each scenario, we have have considered three main topologies:
* Small Topology - composed of 18 nodes including 8 users (consumers and attackers), 9 routers and 1 producer
* DFN Topology - composed of a set of 29 nodes including 12 users (consumers and attackers), 11 routers and 6 producers
* Large-scale Topology - composed of 163 nodes

In the *IFA_4_Existing* scenario, for each topology, we have simulated two main attacking cases:
* fixed_attackers: the number and the position of attackers is kept fixed for all the 5 runs.
  - Consumers' frequency is uniformly selected in [50, 150] requests/second
  - Attackers' frequency is uniformly selected in ranges 4x[50, 150],8x[50, 150], 16x[50, 150], 32x[50, 150] and 64x[50, 150]  
  - Number of attackers is 4 both *Small topology* and *DFN topology*.
* variable_attackers: the number of attackers is fixed and their position changes from one run to the other (max of 5 runs)
  - Consumers' frequency is uniformly selected in [50, 150] requests/second
  - Attackers' frequency is uniformly selected in ranges 4x[50, 150],8x[50, 150], 16x[50, 150], 32x[50, 150] and 64x[50, 150]
  - Number of attackers for *small topology* is 4, 5, 6, 7
  - Number of attackers for *DFN topology* is 4, 8, 11
