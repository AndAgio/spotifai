# any empty lines and lines starting with '#' symbol is ignored

# The file should contain exactly two sections: router and link, each starting with the corresponding keyword

# router section defines topology nodes and their relative positions (e.g., to use in visualizer)
router

# each line in this section represents one router and should have the following data
# node  comment     yPos    xPos
Routbb-165	NA	0	0
Routbb-142	NA	0	0
Routbb-160	NA	0	0
Routbb-161	NA	0	0
Routbb-179	NA	0	0
Routbb-180	NA	0	0
Routbb-163	NA	0	0
Routbb-168	NA	0	0
Routbb-169	NA	0	0
Routbb-167	NA	0	0
Routbb-277	NA	0	0
Routbb-183	NA	0	0
Routbb-186	NA	0	0
Routbb-185	NA	0	0
Routbb-241	NA	0	0
Routbb-210	NA	0	0
Routbb-212	NA	0	0
Routbb-216	NA	0	0
Routbb-209	NA	0	0
Routbb-301	NA	0	0
Routbb-306	NA	0	0
Routbb-243	NA	0	0
Routbb-318	NA	0	0
Routgw-123	NA	0	0
Routgw-125	NA	0	0
Routgw-178	NA	0	0
Routgw-195	NA	0	0
Routgw-127	NA	0	0
Routgw-150	NA	0	0
Routgw-152	NA	0	0
Routgw-257	NA	0	0
Routgw-282	NA	0	0
Routgw-133	NA	0	0
Routgw-134	NA	0	0
Routgw-202	NA	0	0
Routgw-262	NA	0	0
Routgw-236	NA	0	0
Routgw-237	NA	0	0
Routgw-203	NA	0	0
Routgw-200	NA	0	0
Routgw-201	NA	0	0
Routgw-137	NA	0	0
Routgw-138	NA	0	0
Routgw-140	NA	0	0
Routgw-141	NA	0	0
Routgw-228	NA	0	0
Routgw-170	NA	0	0
Routgw-227	NA	0	0
Routgw-147	NA	0	0
Routgw-149	NA	0	0
Routgw-158	NA	0	0
Routgw-156	NA	0	0
Routgw-159	NA	0	0
Routgw-162	NA	0	0
Routgw-244	NA	0	0
Routgw-181	NA	0	0
Routgw-275	NA	0	0
Routgw-276	NA	0	0
Routgw-166	NA	0	0
Routgw-171	NA	0	0
Routgw-173	NA	0	0
Routgw-172	NA	0	0
Routgw-174	NA	0	0
Routgw-182	NA	0	0
Routgw-239	NA	0	0
Routgw-285	NA	0	0
Routgw-242	NA	0	0
Routgw-229	NA	0	0
Routgw-213	NA	0	0
Routgw-187	NA	0	0
Routgw-188	NA	0	0
Routgw-218	NA	0	0
Routgw-207	NA	0	0
Routgw-217	NA	0	0
Routgw-255	NA	0	0
Routgw-214	NA	0	0
Routgw-245	NA	0	0
Routgw-222	NA	0	0
Routgw-208	NA	0	0
Routgw-295	NA	0	0
Routgw-312	NA	0	0
Routgw-219	NA	0	0
Routgw-304	NA	0	0
Routgw-293	NA	0	0
Routgw-234	NA	0	0
Routgw-303	NA	0	0
Routgw-235	NA	0	0
Routgw-247	NA	0	0
Routgw-316	NA	0	0
Routgw-317	NA	0	0
Routgw-279	NA	0	0
User-124	NA	0	0
User-132	NA	0	0
User-135	NA	0	0
User-164	NA	0	0
User-154	NA	0	0
User-126	NA	0	0
User-128	NA	0	0
User-151	NA	0	0
User-274	NA	0	0
User-130	NA	0	0
User-176	NA	0	0
User-196	NA	0	0
User-261	NA	0	0
User-264	NA	0	0
User-139	NA	0	0
User-143	NA	0	0
User-144	NA	0	0
User-145	NA	0	0
User-146	NA	0	0
User-226	NA	0	0
User-157	NA	0	0
User-197	NA	0	0
User-249	NA	0	0
User-250	NA	0	0
User-153	NA	0	0
User-155	NA	0	0
User-177	NA	0	0
User-175	NA	0	0
User-198	NA	0	0
User-199	NA	0	0
User-184	NA	0	0
User-286	NA	0	0
User-189	NA	0	0
User-193	NA	0	0
User-289	NA	0	0
User-252	NA	0	0
User-291	NA	0	0
User-220	NA	0	0
User-215	NA	0	0
User-206	NA	0	0
User-294	NA	0	0
User-221	NA	0	0
User-314	NA	0	0
User-305	NA	0	0
User-253	NA	0	0
User-254	NA	0	0
User-307	NA	0	0
User-231	NA	0	0
User-302	NA	0	0
User-309	NA	0	0
User-232	NA	0	0
User-287	NA	0	0
User-230	NA	0	0
User-233	NA	0	0
User-310	NA	0	0
User-238	NA	0	0
User-246	NA	0	0
User-248	NA	0	0
User-319	NA	0	0
User-278	NA	0	0
User-284	NA	0	0
User-281	NA	0	0
User-280	NA	0	0
User-258	NA	0	0
User-259	NA	0	0
User-265	NA	0	0
User-283	NA	0	0
User-299	NA	0	0
User-300	NA	0	0
User-308	NA	0	0
User-313	NA	0	0
User-311	NA	0	0
# link section defines point-to-point links between nodes and characteristics of these links

link

# Each line should be in the following format (only first two are required, the rest can be omitted)
# srcNode   dstNode     bandwidth   metric  delay   queue
# bandwidth: link bandwidth
# metric: routing metric
# delay:  link delay
# queue:  MaxPackets for transmission queue on the link (both directions)
Routgw-123	User-124	2670300bps	37	38431us	76	
Routgw-123	Routgw-125	14111924bps	7	5082us	401	
Routgw-123	User-126	1880565bps	53	14931us	54	
Routgw-123	Routgw-127	16325849bps	6	8114us	464	
Routgw-123	User-128	1546222bps	64	62794us	44	
Routgw-123	Routgw-150	18171854bps	5	8125us	517	
Routgw-123	User-151	2710491bps	36	42830us	78	
Routgw-123	Routgw-152	16826839bps	5	8004us	479	
User-124	Routgw-178	2284737bps	43	11073us	65	
Routgw-125	Routgw-127	18059193bps	5	7899us	514	
Routgw-125	Routgw-257	19473863bps	5	8078us	554	
Routgw-125	User-274	1376487bps	72	15687us	40	
Routgw-125	Routgw-282	10232595bps	9	8024us	291	
User-126	Routgw-127	1120323bps	89	43455us	32	
User-126	Routgw-282	1204419bps	83	17627us	35	
Routgw-127	User-128	1642752bps	60	19809us	47	
User-128	Routgw-257	2152766bps	46	19922us	62	
User-130	Routgw-133	2417639bps	41	23314us	69	
User-132	Routgw-134	2996520bps	33	29469us	86	
User-132	Routgw-202	1879097bps	53	65756us	54	
User-132	Routgw-262	1504951bps	66	15501us	43	
Routgw-133	Routgw-134	15449603bps	6	9796us	439	
Routgw-133	Routgw-195	17262627bps	5	7736us	491	
Routgw-133	Routgw-202	13151545bps	7	9979us	374	
Routgw-133	Routgw-203	12994371bps	7	9285us	370	
Routgw-133	User-261	2167249bps	46	63901us	62	
Routgw-133	Routgw-262	13457588bps	7	9233us	383	
Routgw-133	User-264	1660692bps	60	19037us	48	
Routgw-134	Routgw-236	16005130bps	6	5053us	455	
Routgw-134	Routgw-237	17851309bps	5	8494us	508	
Routgw-134	User-261	2905339bps	34	33050us	83	
Routgw-134	Routgw-262	14634251bps	6	9983us	416	
User-135	Routgw-200	2168130bps	46	15941us	62	
User-135	Routgw-201	2635305bps	37	34880us	75	
Routgw-137	User-139	2956447bps	33	62701us	84	
Routgw-137	Routgw-140	16827505bps	5	5119us	479	
Routgw-137	Routgw-141	10554499bps	9	5953us	300	
Routgw-137	Routbb-142	13151956bps	7	5820us	374	
Routgw-137	User-143	2451947bps	40	41972us	70	
Routgw-137	User-144	1559852bps	64	48146us	45	
Routgw-137	Routgw-195	11514474bps	8	9436us	328	
Routgw-137	Routgw-202	11275207bps	8	5733us	321	
Routgw-137	Routgw-203	10313961bps	9	6650us	294	
Routgw-137	Routgw-228	15506935bps	6	5793us	441	
Routgw-138	Routgw-140	14024357bps	7	8340us	399	
Routgw-138	Routgw-141	13031174bps	7	5444us	371	
Routgw-138	Routbb-142	19634963bps	5	9497us	558	
Routgw-138	User-143	2920859bps	34	21553us	83	
Routgw-138	Routgw-228	10904037bps	9	7605us	310	
User-139	Routgw-170	1325233bps	75	30349us	38	
User-139	Routgw-227	2828449bps	35	29969us	81	
Routgw-140	User-144	1921858bps	52	19335us	55	
Routgw-140	User-145	2193595bps	45	50536us	63	
Routgw-140	User-146	1690485bps	59	34432us	49	
Routgw-140	Routgw-147	19758027bps	5	5042us	562	
Routgw-140	Routgw-170	14567456bps	6	5754us	414	
Routgw-140	User-226	1348199bps	74	37512us	39	
Routgw-140	Routgw-227	11068776bps	9	8879us	315	
Routgw-141	User-144	2952357bps	33	69258us	84	
Routgw-141	User-145	1476233bps	67	35059us	42	
Routgw-141	User-146	1104335bps	90	51099us	32	
Routgw-141	Routgw-147	14355781bps	6	5544us	408	
Routgw-141	Routgw-170	14890604bps	6	6616us	424	
Routbb-142	Routgw-147	17989237bps	5	7036us	512	
Routbb-142	Routgw-170	17805425bps	5	6829us	506	
Routbb-142	Routgw-227	16244761bps	6	8876us	462	
Routgw-147	User-164	1281955bps	78	17750us	37	
Routgw-147	Routbb-165	17742593bps	5	6211us	505	
Routgw-149	User-151	2597077bps	38	56986us	74	
Routgw-149	Routgw-152	19109580bps	5	8415us	543	
Routgw-149	Routgw-158	11357986bps	8	8824us	323	
Routgw-149	User-249	1947504bps	51	42434us	56	
Routgw-149	User-250	2296681bps	43	21735us	66	
Routgw-150	Routgw-152	17702647bps	5	9686us	503	
Routgw-150	Routgw-156	10805217bps	9	5869us	307	
Routgw-150	User-157	1759682bps	56	37428us	50	
Routgw-150	Routgw-158	13670954bps	7	9702us	389	
Routgw-150	User-249	1965069bps	50	40202us	56	
Routgw-150	User-250	1512172bps	66	45621us	43	
Routgw-152	User-153	1452987bps	68	20063us	42	
User-154	Routgw-158	2325792bps	42	50689us	67	
User-154	Routgw-159	2966710bps	33	26637us	85	
User-154	Routgw-162	1885325bps	53	58723us	54	
User-155	Routgw-158	2396520bps	41	67190us	69	
User-155	Routgw-162	2468153bps	40	16307us	71	
Routgw-156	User-157	1529352bps	65	66436us	44	
Routgw-156	Routgw-158	10329402bps	9	6224us	294	
Routgw-156	Routgw-159	18909979bps	5	9799us	538	
Routgw-156	Routgw-162	15890041bps	6	8273us	452	
User-157	Routgw-244	2884665bps	34	64839us	82	
Routgw-158	Routgw-159	18232077bps	5	9982us	518	
Routgw-158	Routgw-244	13130553bps	7	8274us	374	
Routgw-159	Routgw-282	17016179bps	5	7162us	484	
Routbb-160	Routgw-162	16097190bps	6	7680us	458	
Routbb-160	Routgw-181	15827297bps	6	5170us	450	
Routbb-160	Routgw-236	13461982bps	7	8872us	383	
Routbb-160	Routgw-237	10981002bps	9	5415us	312	
Routbb-160	Routgw-275	15482747bps	6	5624us	440	
Routbb-160	Routgw-276	17648324bps	5	8152us	502	
Routbb-161	Routgw-162	19528187bps	5	9109us	555	
Routbb-161	Routbb-179	46876698bps	2	8060us	1332	
Routbb-161	Routbb-180	60765600bps	1	5600us	1727	
Routbb-161	Routgw-181	19411261bps	5	6689us	552	
Routbb-161	Routgw-236	15389658bps	6	8728us	438	
Routbb-161	Routgw-237	18167525bps	5	7984us	517	
Routbb-161	Routgw-275	10418876bps	9	7642us	296	
Routbb-161	Routgw-276	11302133bps	8	8703us	322	
Routgw-162	Routbb-168	13880891bps	7	7232us	395	
Routgw-162	Routbb-169	13376176bps	7	7008us	381	
Routgw-162	Routgw-275	15234557bps	6	5218us	433	
Routgw-162	Routgw-276	15975499bps	6	9509us	454	
Routbb-163	Routbb-168	87240745bps	1	9859us	2479	
Routbb-163	Routbb-169	55761651bps	1	9139us	1585	
Routbb-163	Routgw-170	17731529bps	5	5455us	504	
Routbb-163	Routgw-171	18100240bps	5	6707us	515	
Routbb-163	Routgw-173	12331710bps	8	9489us	351	
User-164	Routgw-166	2818392bps	35	21764us	81	
Routbb-165	Routbb-167	54405341bps	1	9193us	1546	
Routbb-165	Routbb-168	91689348bps	1	5460us	2605	
Routbb-165	Routbb-169	66998292bps	1	9380us	1904	
Routbb-165	Routgw-171	19115840bps	5	8018us	544	
Routbb-165	Routbb-277	69367736bps	1	9345us	1971	
Routgw-166	Routbb-168	16188519bps	6	6078us	460	
Routgw-166	Routbb-169	12882940bps	7	6633us	366	
Routgw-166	Routbb-277	18985596bps	5	8819us	540	
Routbb-167	Routbb-168	46446179bps	2	6919us	1320	
Routbb-167	Routbb-169	61538482bps	1	7435us	1749	
Routbb-167	Routgw-170	14123187bps	7	7655us	402	
Routbb-167	Routgw-171	14739877bps	6	7425us	419	
Routbb-167	Routgw-275	18382380bps	5	7694us	523	
Routbb-168	Routgw-170	18092349bps	5	5495us	514	
Routbb-168	Routgw-172	19622880bps	5	7302us	558	
Routbb-168	Routgw-174	19931735bps	5	7420us	567	
Routbb-168	Routgw-236	11666068bps	8	9334us	332	
Routbb-168	Routgw-237	14602869bps	6	6372us	415	
Routbb-169	Routgw-172	12761629bps	7	5141us	363	
Routbb-169	Routgw-173	19870156bps	5	9084us	565	
Routbb-169	Routgw-174	13096681bps	7	5559us	373	
Routbb-169	Routgw-236	17718204bps	5	6414us	504	
Routbb-169	Routgw-237	12416256bps	8	8596us	353	
Routbb-169	Routbb-277	92350605bps	1	5858us	2624	
Routgw-170	Routgw-172	12255982bps	8	9534us	349	
Routgw-170	Routgw-174	17661774bps	5	9037us	502	
Routgw-170	Routgw-178	12405619bps	8	9915us	353	
Routgw-170	Routgw-200	11993734bps	8	8727us	341	
Routgw-171	Routgw-172	19661219bps	5	8436us	559	
Routgw-171	Routgw-173	16295386bps	6	9076us	463	
Routgw-171	Routgw-174	13428105bps	7	5831us	382	
Routgw-171	User-177	1479970bps	67	67432us	43	
Routgw-171	Routgw-178	18089803bps	5	9517us	514	
Routgw-171	User-198	1977615bps	50	49791us	57	
Routgw-171	User-199	2600736bps	38	60575us	74	
Routgw-171	Routgw-200	18556749bps	5	6911us	528	
Routgw-171	Routgw-201	13641846bps	7	9878us	388	
Routgw-171	Routbb-277	17154152bps	5	9360us	488	
Routgw-172	Routgw-173	17358844bps	5	9541us	494	
Routgw-172	Routgw-174	13998554bps	7	7172us	398	
Routgw-172	User-175	1670142bps	59	15909us	48	
Routgw-173	User-175	1232943bps	81	30170us	36	
Routgw-174	User-175	1514305bps	66	44278us	44	
User-176	Routgw-178	2555019bps	39	52123us	73	
Routgw-178	Routgw-282	17409118bps	5	8680us	495	
Routbb-179	Routgw-182	16279638bps	6	8420us	463	
Routbb-179	Routbb-183	67244210bps	1	6570us	1911	
Routbb-179	Routbb-186	76222940bps	1	8908us	2166	
Routbb-179	Routgw-239	16144102bps	6	5631us	459	
Routbb-180	Routgw-182	15208155bps	6	9452us	433	
Routbb-180	Routbb-183	40512379bps	2	8181us	1151	
Routbb-180	Routbb-185	74577101bps	1	8539us	2119	
Routbb-180	Routbb-186	82223656bps	1	5892us	2336	
Routbb-180	Routgw-239	14760108bps	6	5078us	420	
Routbb-180	Routgw-285	12608475bps	7	5689us	359	
Routgw-181	Routbb-183	17049080bps	5	7423us	485	
Routgw-181	User-184	2426712bps	41	43373us	69	
Routgw-181	Routbb-185	16751813bps	5	9800us	476	
Routgw-181	Routbb-186	13783976bps	7	5804us	392	
Routgw-181	Routgw-239	11047056bps	9	6366us	314	
Routgw-182	Routbb-185	19216393bps	5	7973us	546	
Routgw-182	Routbb-186	16735783bps	5	8501us	476	
Routgw-182	Routgw-242	19507978bps	5	8256us	555	
Routgw-182	Routgw-285	16919313bps	5	6658us	481	
Routgw-182	User-286	1592164bps	62	56426us	46	
Routbb-183	Routbb-185	92274553bps	1	7128us	2622	
Routbb-183	Routbb-241	41685268bps	2	5491us	1185	
Routbb-183	Routgw-242	13953736bps	7	5865us	397	
Routgw-187	Routgw-188	18651958bps	5	9317us	530	
Routgw-187	User-189	1990451bps	50	42635us	57	
Routgw-187	Routgw-195	14869843bps	6	7571us	423	
Routgw-187	Routgw-202	10826336bps	9	8982us	308	
Routgw-187	Routgw-203	18433051bps	5	5590us	524	
Routgw-187	User-289	2352267bps	42	57735us	67	
Routgw-188	User-189	1311133bps	76	49007us	38	
Routgw-188	Routgw-229	12625061bps	7	7110us	359	
Routgw-188	User-289	1790361bps	55	33502us	51	
User-193	Routgw-200	1053921bps	94	51105us	30	
User-193	Routgw-203	2783997bps	35	69776us	80	
Routgw-195	User-196	2731432bps	36	42843us	78	
Routgw-195	Routgw-201	18592928bps	5	9435us	529	
Routgw-195	Routgw-218	16392161bps	6	9506us	466	
User-196	Routgw-203	2386638bps	41	53689us	68	
User-197	Routgw-202	1410018bps	70	32669us	41	
User-197	Routgw-203	1210924bps	82	49568us	35	
User-198	Routgw-213	2209889bps	45	40331us	63	
User-199	Routgw-213	2484508bps	40	69052us	71	
Routgw-200	Routgw-213	15464024bps	6	9034us	440	
Routgw-200	User-291	2334812bps	42	69937us	67	
Routgw-201	Routgw-202	11210959bps	8	8238us	319	
Routgw-201	Routgw-203	19237066bps	5	9097us	547	
Routgw-201	Routbb-210	10664047bps	9	5554us	303	
Routgw-201	Routbb-212	19675060bps	5	6466us	559	
Routgw-201	Routgw-213	10477061bps	9	9492us	298	
Routgw-201	User-291	1594208bps	62	40999us	46	
Routgw-202	Routgw-217	17116042bps	5	5087us	487	
Routgw-202	Routgw-218	18859255bps	5	5727us	536	
Routgw-202	Routgw-255	12262001bps	8	7437us	349	
Routgw-202	User-291	2242793bps	44	46452us	64	
Routgw-203	Routgw-218	10283967bps	9	7419us	293	
Routgw-203	Routgw-255	10248305bps	9	5261us	292	
User-206	Routgw-213	1522367bps	65	51098us	44	
Routgw-207	Routbb-212	10207338bps	9	6940us	290	
Routgw-207	Routgw-214	19246626bps	5	9225us	547	
Routgw-207	User-220	2789545bps	35	47829us	80	
Routgw-207	Routgw-222	13465050bps	7	5379us	383	
Routgw-207	User-294	2882873bps	34	60520us	82	
Routgw-208	Routbb-212	10194942bps	9	8435us	290	
Routgw-208	Routgw-213	16597273bps	6	5841us	472	
Routgw-208	Routgw-214	18081201bps	5	5515us	514	
Routgw-208	User-220	1390579bps	71	57720us	40	
Routgw-208	User-221	2402035bps	41	54728us	69	
Routgw-208	Routgw-222	19174086bps	5	5409us	545	
Routgw-208	Routgw-295	17571686bps	5	6068us	500	
Routgw-208	Routbb-301	14166881bps	7	8250us	403	
Routbb-209	Routbb-210	94573289bps	1	9562us	2687	
Routbb-209	Routbb-212	76168006bps	1	7652us	2164	
Routbb-209	Routgw-213	10570257bps	9	9173us	301	
Routbb-209	Routgw-214	15701771bps	6	5491us	447	
Routbb-209	Routgw-222	11681424bps	8	5660us	332	
Routbb-209	Routgw-295	10574671bps	9	5644us	301	
Routbb-210	Routbb-216	92631960bps	1	8915us	2632	
Routbb-210	Routgw-219	19480612bps	5	6163us	554	
Routbb-210	Routgw-222	11186468bps	8	8472us	318	
Routbb-210	Routbb-306	70756319bps	1	8410us	2011	
Routbb-212	Routbb-216	61438861bps	1	5275us	1746	
Routbb-212	Routgw-219	12289097bps	8	6324us	350	
Routbb-212	Routgw-222	11623073bps	8	8939us	331	
Routgw-213	Routbb-216	11603929bps	8	9165us	330	
Routgw-213	Routgw-217	16295215bps	6	7640us	463	
Routgw-213	Routgw-218	19142477bps	5	9961us	544	
Routgw-213	Routgw-219	15322942bps	6	8426us	436	
Routgw-213	User-221	1693441bps	59	19525us	49	
Routgw-213	Routgw-222	18253998bps	5	8230us	519	
Routgw-213	Routgw-304	15883736bps	6	7472us	452	
Routgw-214	Routgw-214	10482753bps	9	5935us	298	
Routgw-214	User-215	2285420bps	43	17580us	65	
Routgw-214	Routbb-216	12990304bps	7	6188us	370	
Routgw-214	Routgw-217	18345251bps	5	6313us	522	
Routgw-214	Routgw-218	17282332bps	5	6584us	491	
Routgw-214	User-305	2305595bps	43	33713us	66	
Routgw-214	Routbb-306	11346840bps	8	6657us	323	
User-215	Routgw-217	2767978bps	36	66094us	79	
Routbb-216	Routgw-217	16319008bps	6	7056us	464	
Routbb-216	Routgw-218	15786441bps	6	5247us	449	
Routgw-217	Routgw-236	13486771bps	7	6872us	384	
Routgw-217	User-253	2398456bps	41	12397us	69	
Routgw-217	User-254	2374060bps	42	19044us	68	
Routgw-217	Routgw-293	11004156bps	9	8634us	313	
Routgw-217	Routgw-295	18407355bps	5	6998us	523	
Routgw-217	Routbb-301	15064435bps	6	8059us	428	
Routgw-217	User-302	1931445bps	51	19811us	55	
Routgw-217	Routgw-303	14621131bps	6	8445us	416	
Routgw-217	Routgw-304	11343339bps	8	8063us	323	
Routgw-217	Routbb-306	19153312bps	5	8206us	545	
Routgw-218	Routgw-236	17159221bps	5	6119us	488	
Routgw-218	Routgw-237	13756133bps	7	8110us	391	
Routgw-218	User-253	1873303bps	53	21586us	54	
Routgw-218	Routbb-301	12491950bps	8	8893us	355	
Routgw-218	Routgw-303	16078397bps	6	9776us	457	
Routgw-218	Routbb-306	13913317bps	7	8690us	396	
Routgw-219	User-220	2947012bps	33	11319us	84	
Routgw-222	User-307	2064360bps	48	17413us	59	
Routgw-227	User-287	1038973bps	96	36577us	30	
Routgw-228	Routgw-229	10279498bps	9	5587us	293	
Routgw-228	User-309	2400101bps	41	47798us	69	
Routgw-229	User-230	1040788bps	96	49128us	30	
Routgw-229	User-309	1140234bps	87	37552us	33	
User-231	Routgw-234	2857795bps	34	33531us	82	
User-232	Routgw-234	1386857bps	72	60551us	40	
User-233	Routgw-234	1606952bps	62	29182us	46	
Routgw-234	Routgw-235	16428625bps	6	7574us	467	
Routgw-234	Routgw-236	17929889bps	5	5270us	510	
Routgw-234	Routgw-237	11115518bps	8	5182us	316	
Routgw-235	Routgw-236	16782059bps	5	5563us	477	
Routgw-235	Routgw-237	11939056bps	8	8045us	340	
Routgw-235	User-310	1501655bps	66	36372us	43	
Routgw-236	User-238	1619645bps	61	34011us	47	
Routgw-237	User-238	2389997bps	41	28056us	68	
User-238	Routgw-312	2781089bps	35	35746us	80	
Routgw-239	User-314	2923795bps	34	25523us	84	
Routbb-241	Routgw-245	13796158bps	7	8368us	392	
Routbb-241	Routgw-247	13566024bps	7	8644us	386	
Routbb-241	Routgw-316	14552063bps	6	6378us	414	
Routbb-241	Routgw-317	13725057bps	7	5429us	390	
Routbb-241	Routbb-318	71605399bps	1	7436us	2035	
Routgw-242	Routbb-243	12762461bps	7	5962us	363	
Routgw-242	Routgw-244	17044302bps	5	8462us	485	
Routgw-242	Routgw-245	18985666bps	5	7968us	540	
Routgw-242	User-246	2221843bps	45	56678us	64	
Routgw-242	Routgw-247	15559267bps	6	5835us	443	
Routgw-242	Routgw-316	18929359bps	5	8996us	538	
Routgw-242	Routgw-317	16988304bps	5	5573us	483	
Routgw-242	Routbb-318	12542675bps	7	7299us	357	
Routbb-243	Routgw-245	10320558bps	9	5592us	294	
Routbb-243	Routgw-247	18678205bps	5	7289us	531	
Routbb-243	Routbb-318	89146828bps	1	7933us	2533	
Routgw-244	Routgw-245	13341608bps	7	7353us	380	
Routgw-244	Routgw-247	18145635bps	5	5932us	516	
Routgw-244	Routgw-316	15517696bps	6	5059us	441	
Routgw-244	Routbb-318	16345328bps	6	7481us	465	
Routgw-244	User-319	1691260bps	59	10959us	49	
Routgw-245	User-248	2286710bps	43	62678us	65	
Routgw-247	User-248	1296765bps	77	39366us	37	
User-252	Routgw-255	1803814bps	55	31529us	52	
User-254	Routgw-255	1429103bps	69	21368us	41	
Routgw-257	User-274	2680811bps	37	47515us	77	
Routgw-257	Routgw-282	18029832bps	5	8635us	513	
User-258	Routgw-312	1554815bps	64	13659us	45	
User-259	Routgw-262	2202455bps	45	24047us	63	
Routgw-262	User-264	2934721bps	34	23855us	84	
User-265	Routgw-275	1989922bps	50	68932us	57	
Routgw-275	Routgw-276	16983546bps	5	8861us	483	
Routgw-275	User-278	1067046bps	93	65663us	31	
Routgw-275	Routgw-279	17977411bps	5	6958us	511	
Routgw-275	Routgw-282	11784139bps	8	5968us	335	
Routgw-275	User-283	2071022bps	48	50082us	59	
Routgw-276	Routgw-279	19296591bps	5	5499us	549	
Routgw-276	User-281	1501286bps	66	17532us	43	
Routgw-279	User-280	1516764bps	65	18383us	44	
Routgw-279	User-281	2329735bps	42	66691us	67	
User-284	Routgw-285	1126983bps	88	19225us	33	
User-284	Routgw-317	2841160bps	35	46449us	81	
Routgw-285	Routgw-316	19586092bps	5	6171us	557	
Routgw-285	Routgw-317	13615094bps	7	6264us	387	
User-286	Routgw-317	2215627bps	45	20397us	63	
Routgw-293	User-300	2536971bps	39	12595us	73	
Routgw-293	Routbb-301	14159745bps	7	5191us	403	
Routgw-293	Routgw-303	11355898bps	8	7005us	323	
Routgw-293	Routgw-304	14510540bps	6	6896us	413	
Routgw-295	User-299	1306493bps	76	50428us	38	
Routgw-295	Routgw-303	12506536bps	7	9055us	356	
Routgw-295	Routgw-304	11817360bps	8	6255us	336	
Routgw-295	Routbb-306	12701261bps	7	6207us	361	
Routbb-301	Routgw-304	12353966bps	8	8929us	351	
Routbb-301	Routbb-306	89135877bps	1	5394us	2533	
User-302	Routgw-303	1079714bps	92	68744us	31	
User-302	Routgw-304	2972748bps	33	69064us	85	
Routgw-303	Routgw-304	19049340bps	5	5030us	542	
Routgw-304	Routbb-306	18660674bps	5	6337us	531	
Routgw-304	User-308	1608562bps	62	31798us	46	
User-310	Routgw-312	1442766bps	69	41487us	41	
User-311	Routgw-312	2219966bps	45	51771us	64	
User-313	Routgw-316	2415370bps	41	47592us	69	
Routgw-316	Routgw-317	19563595bps	5	9600us	556	
Routbb-167	Routbb-142	73413242bps	1	8806us	2086	
Routbb-142	Routbb-160	57683390bps	1	9288us	1639	
Routbb-160	Routbb-243	45654169bps	2	9546us	1297	
Routbb-161	Routbb-209	55465977bps	1	6080us	1576	
